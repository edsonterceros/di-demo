package com.dh.didemo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dh.didemo.services.GreetingServiceImpl;

/**
 * PropertyBasedController Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Dec 12, 2017</pre>
 */
public class PropertyBasedControllerTest {

    PropertyBasedController propertyBasedController;
    @Before
    public void before() throws Exception {
        propertyBasedController = new PropertyBasedController();
        propertyBasedController.greetingService= new GreetingServiceImpl();
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: sayHello()
     */
    @Test
    public void testSayHello() throws Exception {
        assertEquals(propertyBasedController.sayHello(),GreetingServiceImpl.HELLO_GREETING_SERVICE);
    }

} 
