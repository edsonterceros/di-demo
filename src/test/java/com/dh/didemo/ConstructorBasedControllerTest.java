package com.dh.didemo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dh.didemo.services.GreetingServiceImpl;

/**
 * ConstructorBasedController Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Dec 12, 2017</pre>
 */
public class ConstructorBasedControllerTest {

    private ConstructorBasedController constructorBasedController;
    @Before
    public void before() throws Exception {
        constructorBasedController= new ConstructorBasedController(new GreetingServiceImpl());
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: sayHello()
     */
    @Test
    public void testSayHello() throws Exception {
        assertEquals(constructorBasedController.sayHello(), GreetingServiceImpl.HELLO_GREETING_SERVICE);
    }

} 
