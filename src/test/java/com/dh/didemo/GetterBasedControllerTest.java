package com.dh.didemo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dh.didemo.services.GreetingServiceImpl;

/**
 * PropertyBasedController Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Dec 12, 2017</pre>
 */
public class GetterBasedControllerTest {

    private GetterBasedController getterBasedController;
    @Before
    public void before() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceImpl());
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: sayHello()
     */
    @Test
    public void testSayHello() throws Exception {
        assertEquals(getterBasedController.sayHello(),GreetingServiceImpl.HELLO_GREETING_SERVICE);
    }

} 
