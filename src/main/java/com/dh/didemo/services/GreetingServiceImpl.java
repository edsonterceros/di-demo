/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.didemo.services;

public class GreetingServiceImpl implements GreetingService{

    public static final String HELLO_GREETING_SERVICE = "Hello GreetingService";

    @Override
    public String sayGreeting() {
        return HELLO_GREETING_SERVICE;
    }
}
