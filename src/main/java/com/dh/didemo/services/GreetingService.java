/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.didemo.services;

public interface GreetingService {
    String sayGreeting();
}
