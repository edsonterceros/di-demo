/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.didemo;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {

    private static final String HELLO_SPRING = "Hello Spring";

    public String hello(){
        System.out.println(HELLO_SPRING);
        return HELLO_SPRING;
    }
}
